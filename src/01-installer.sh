LAST_DEBUG=""

## FUNCTIONS
debug(){
  LAST_DEBUG=$1
  echo ""
  echo ""
  echo "--[ $1 ]------------------------------"
  echo ""

  sleep 1
}


checkResult(){
  if [ "$?" != "0" ]; then
    debug "Ukoncuji skript kvuli chybe!"
    exit 1
  else
    echo "[OK] ${1:-$LAST_DEBUG}"
  fi
  LAST_DEBUG="${1:-$LAST_DEBUG}"
}


## APP
if test $EUID -ne 0; then
  echo "Tento skript musi byt spusten jako root"
  exit 1
fi

echo ""
echo "# Priprava:"
echo ""
read -p "Domena (napr.: sspbrno.cz):" domain

echo "#####"
echo "# Skript nyni bude pokracovat samostatne..."
echo "#####"
sleep 3


echo ""
echo "-- Priprava --"
echo ""

debug "Pridavam repositare"
curl -sL https://deb.nodesource.com/setup_lts.x | bash -
checkResult

debug "Aktualizuji systém"
apt-get upgrade -y
checkResult

debug "Instaluji potrebne zavislosti"
apt install -y gcc g++ make nginx git certbot nodejs
checkResult

debug "Mazu nepotrebne balicky"
apt remove --purge apache2

mkdir -p ~/.npm
chown -R $(whoami) ~/.npm

echo ""
echo "-- Stahovani --"
echo ""

debug "Stahuji sspbrno-web z repozitare"
cd /srv/
rm -r sspbrno-web
git clone --recurse-submodules \
    https://Zhincore@bitbucket.org/purkynka-renewal/sspbrno-web.git
checkResult

echo ""
echo "-- Instalace --"
echo ""

debug "Instaluji knihovny"
cd sspbrno-web
checkResult
sudo npm install
checkResult
sudo npm audit fix
checkResult
sudo npm run install
checkResult


echo ""
echo "-- Konfigurace --"
echo ""

debug "Konfiguruji kernel"
echo -e $SYSCTL_CONF > /etc/sysctl.conf
checkResult

debug "Konfiguruji NGINX"
mkdir -p /etc/nginx
echo -e $NGINX_CONF > /etc/nginx/nginx.conf
checkResult

debug "Konfiguruji vhost pro NGINX"
mkdir -p /etc/nginx/sites-enabled
rm /etc/nginx/sites-enabled/*
echo -e $NGINX_VHOST > /etc/nginx/sites-enabled/default
checkResult

debug "Konfiguruji SystemD"
echo -e $SERVICE_CONF > /usr/lib/systemd/system/sspbrno-web.service
checkResult


echo ""
echo "-- Certifikat --"
echo ""

systemctl stop nginx

debug "Ziskavam certifikat"
certbot certonly -n --standalone --agree-tos \
    -d "$domain" --register-unsafely-without-email
checkResult

ln "/etc/letsencrypt/archive/$domain/fullchain1.pem" "data/fullchain.pem"
checkResult
ln "/etc/letsencrypt/archive/$domain/privkey1.pem" "data/privkey.pem"
checkResult


echo ""
echo "-- Dokoncovani --"
echo ""

debug "Nastavuji opravneni"
chown -R www-data:www-data /srv/
checkResult
chmod -R u+rwx,g+rwxs,o+rx-w /srv/
checkResult
chmod -R u+rwx,g+rwxs,o-rwx /srv/sspbrno-web/data
checkResult


debug "Nastavuji sluzby"
systemctl disable apache2
systemctl enable nginx
checkResult
systemctl enable sspbrno-web
checkResult


debug "INSTALACE DOKONCENA"
echo "#####"
echo "# Nyni je nutno restartovat system prikazem 'reboot'"
echo "#####"
