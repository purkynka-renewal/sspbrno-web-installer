user www-data;
worker_processes auto;
pid /run/nginx.pid;
include /etc/nginx/modules-enabled/*.conf;

events {
  worker_connections 768;
}

http {
  ##
  # Basic Settings
  ##
  sendfile on;
  sendfile_max_chunk 1m;
  tcp_nopush on;
  tcp_nodelay on;

  resolver 8.8.8.8 8.8.4.4 valid=300s;
  resolver_timeout 5s;

  open_file_cache          max=10000 inactive=5m;
  open_file_cache_valid    2m;
  open_file_cache_min_uses 1;
  open_file_cache_errors   on;

  ##
  # Security Settings
  ##
  server_tokens off;

  # Overflow protection
  client_body_buffer_size  	1K;
  client_header_buffer_size 1k;
  client_max_body_size 	1k;
  large_client_header_buffers 2 1k;
  types_hash_max_size 	2048;

  # Spam protection
  reset_timedout_connection on;
  client_body_timeout   10;
  client_header_timeout 10;
  keepalive_timeout     20;
  send_timeout          10;

  http2_push_preload on;

  include /etc/nginx/mime.types;
  default_type application/octet-stream;

  ##
  # SSL Settings
  ##
  ssl_protocols TLSv1 TLSv1.1 TLSv1.2; # Dropping SSLv3, ref: POODLE
  ssl_prefer_server_ciphers on;

  ##
  # Logging Settings
  ##
  access_log /var/log/nginx/access.log buffer=32k;
  error_log /var/log/nginx/error.log;

  ##
  # Gzip Settings
  ##
  gzip on;
  gzip_vary on;
  gzip_proxied any;
  gzip_comp_level 6;
  gzip_buffers 16 8k;
  gzip_types text/plain text/css application/json application/javascript text/xml application/xml application/xml+rss text/javascript image/svg+xml;


  ##
  # Virtual Host Configs
  ##

  # Prevent access from foreign domains
  # TODO: add domain name to config from install script
  # server {
  # 	listen 80 default_server;
  # 	listen [::]:80 default_server ipv6only=on;
  # 	server_name _;
  # 	return 301 https://$host$request_uri;
  # }

  # Redirect http to https
  server {
    listen 80 default_server;
    listen [::]:80 default_server ipv6only=on;
    server_name _; # TODO: change to domain name
    return 301 https://$host$request_uri;
  }

  include /etc/nginx/conf.d/*.conf;
  include /etc/nginx/sites-enabled/*;
}
