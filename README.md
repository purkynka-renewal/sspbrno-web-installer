# SSPBrno-web installer
Nainstaluje a připraví sspbrno-web, nginx a vše potřebné pro nejlepší výkon a
bezpečnost nového webu

---

## Funkce
+ Nainstaluje závislosti
+ Nainstaluje NodeJS LTS repositář a NodeJS LTS samotné
+ Stáhne sspbrno-web z GIT repositáře
+ Nainstaluje knihovny pro sspbrno-web pomocí NPM
+ Nakonfiguruje NGINX, SystemD a kernel pro nejlepší výkon a bezpečnost

## Build
+ Spusťte `build.sh`
+ Výsledný skript je `dist/installer.sh`

Hotový skript lze také stáhnout z <https://bitbucket.org/purkynka-renewal/sspbrno-web-installer/downloads/installer.sh>

## Potřebnosti
+ Čistá instalace Ubuntu 20.04 LTS
+ Již připravená doména nasměrovaná na server, na který chcete sspbrno-web
instalovat
+ Otevřený port `443 (https)` a `80 (http)`, který bude vždy přesměrován na port
`443 (https)`


## Použití
+ Skript `installer.sh` umístěte někam na server a spusťte ho jako root nebo se
`sudo`
+ Kvůli potřebnému SSL certifikátu budete dotázáni na doménu pod kterou server
běží
+ Poté již skript běží sám a pokud vše proběhne jak má, budete požádáni o
restart serveru. Po restartu by měl být server připravený na používaní

## Autor
Adam Žingor <https://zhincore.eu/> 2020
