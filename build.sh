#!/usr/bin/env bash


#
# FUNCTIONS
#

minifyConf(){
  echo -n "$1=\$(echo \"$(
    cat "$2" \
    | sed 's/$/\\n/gm' \
    | sed ':a;N;$!ba;s/\n//g' \
    | base64 -w 0 -
  )\" | base64 -d -); "
}

minifySh(){
  cat "$1" \
  | sed 's/^#.*$//gm' \
  | sed -r 's/^[[:blank:]]+//g; s/[[:blank:]]+$//g; s/[[:blank:]]+\\$/\\/g' \
  | sed '/^[[:blank:]]*$/d' \
  | sed 's/$/;/; s/{;$/{/; s/then;$/then/; s/else;$/else/; s/\\;//g' \
  | sed ':a;N;$!ba; s/\n/ /g'
}


#
# APP
#

# cd to scripts dir
cd $(dirname $(realpath "$0"))

mkdir -p dist

# Prepare file
echo "#!/usr/bin/env bash" > dist/installer.sh

# include confs
minifyConf "SYSCTL_CONF"  "src/00-sysctl.conf"          >> dist/installer.sh
minifyConf "NGINX_CONF"   "src/00-nginx.conf"           >> dist/installer.sh
minifyConf "NGINX_VHOST"  "src/00-nginx-default"        >> dist/installer.sh
minifyConf "SERVICE_CONF" "src/00-sspbrno-web.service"  >> dist/installer.sh

# include scripts
minifySh "src/01-installer.sh"  >> dist/installer.sh
